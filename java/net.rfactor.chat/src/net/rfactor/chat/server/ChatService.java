package net.rfactor.chat.server;

public interface ChatService {
    /** The unique identity for the settings of this service. */
    public static final String CHATSERVICE_PID = "chat";
    /** Key for the server name. */
    public static final String SERVERNAME_KEY = "servername";
    
    public void sendMessage(String message);
    
    public void setPause(boolean pause);
    
    public boolean getPause();
}
