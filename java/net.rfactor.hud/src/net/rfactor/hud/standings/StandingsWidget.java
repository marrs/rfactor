package net.rfactor.hud.standings;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import net.ctdp.rfdynhud.gamedata.LiveGameData;
import net.ctdp.rfdynhud.input.InputAction;
import net.ctdp.rfdynhud.properties.ColorProperty;
import net.ctdp.rfdynhud.properties.PropertiesContainer;
import net.ctdp.rfdynhud.properties.PropertyLoader;
import net.ctdp.rfdynhud.properties.StringProperty;
import net.ctdp.rfdynhud.render.DrawnString;
import net.ctdp.rfdynhud.render.DrawnString.Alignment;
import net.ctdp.rfdynhud.render.DrawnStringFactory;
import net.ctdp.rfdynhud.render.TextureImage2D;
import net.ctdp.rfdynhud.util.PropertyWriter;
import net.ctdp.rfdynhud.util.SubTextureCollector;
import net.ctdp.rfdynhud.valuemanagers.Clock;
import net.ctdp.rfdynhud.values.IntValue;
import net.ctdp.rfdynhud.values.StringValue;
import net.ctdp.rfdynhud.widgets.base.widget.Widget;
import net.rfactor.hud.widgetset.EnduranceWidgetSet;

import org.jagatoo.logging.LogLevel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 */
public class StandingsWidget extends Widget implements Runnable {
    private static final int UPDATE_DELAY = 2000;
	private final Gson m_gson = new Gson();
    private Thread m_thread;
    private int m_rowHeight;
    private int m_iconOffsetX;
    private DrawnString[] m_strings;
    private StringValue[] m_values;
    private IntValue[] m_flagColorValues;
    private final ColorProperty m_myVehicleColor = new ColorProperty( "myColor", "#FFFFFF" );
    private final StringProperty m_serverWidgetEndpoint = new StringProperty("serverURL", "http://localhost:8080/score/widget");
    private static final InputAction TOGGLE_ACTION = new InputAction("ToggleAction", true);
    private enum Displays { STANDINGS, GRID };
    private Displays m_display = Displays.STANDINGS;
    private Displays m_lastDrawnDisplay;
    private ArrayList<Vehicle> vehicles = null;
    
    public StandingsWidget() {
        super(EnduranceWidgetSet.INSTANCE, EnduranceWidgetSet.WIDGET_PACKAGE, 5.0f, 5.0f);
    }

    @Override
    public void saveProperties(PropertyWriter writer) throws IOException {
        super.saveProperties(writer);
        writer.writeProperty(m_myVehicleColor, "The text color for my own vehicle.");
        writer.writeProperty(m_serverWidgetEndpoint, "The URL for the server endpoint the widget should connect to.");
    }
    
    @Override
    public void loadProperty(PropertyLoader loader) {
        super.loadProperty(loader);
        
        if (loader.loadProperty(m_myVehicleColor));
        else if (loader.loadProperty(m_serverWidgetEndpoint));
    }

    @Override
    public void getProperties(PropertiesContainer propsCont, boolean forceAll) {
        super.getProperties(propsCont, forceAll);
        propsCont.addGroup("Standings");
        propsCont.addProperty(m_myVehicleColor);
        propsCont.addProperty(m_serverWidgetEndpoint);
    } 
    
    @Override
    public InputAction[] getInputActions() {
        return (new InputAction[] { TOGGLE_ACTION });
    }

    @Override
    public void prepareForMenuItem() {
        super.prepareForMenuItem();
        getFontProperty().setFont("Dialog", Font.PLAIN, 9, false, true);
    }

    @Override
    public void onRealtimeEntered(LiveGameData gameData, boolean isEditorMode) {
        super.onRealtimeEntered(gameData, isEditorMode);
        if (m_values != null) {
            for (int i = 0; i < m_values.length; i++) {
                m_values[i].reset();
            }
        }
        if (m_flagColorValues != null) {
        	for (int i = 0; i < m_flagColorValues.length; i++) {
                m_flagColorValues[i].reset();
            }
        }
    }
    
    @Override
    protected Boolean onBoundInputStateChanged(InputAction action, boolean state, int modifierMask, long when, LiveGameData gameData, boolean isEditorMode) {
        Boolean result = super.onBoundInputStateChanged(action, state, modifierMask, when, gameData, isEditorMode);

        if (action == TOGGLE_ACTION) {
            // yeah I know, not the smartest toggle in the world
            switch (m_display) {
                case GRID:
                    m_display = Displays.STANDINGS;
                    break;
                case STANDINGS:
                    m_display = Displays.GRID;
                    break;
            }
        }
        return (result);
    }
    
    @Override
    protected void initSubTextures(LiveGameData gameData, boolean isEditorMode, int widgetInnerWidth, int widgetInnerHeight, SubTextureCollector collector) {
    }

    @Override
    protected void initialize(LiveGameData gameData, boolean isEditorMode, DrawnStringFactory drawnStringFactory, TextureImage2D texture, int width, int height) {
        // based on the dimensions, determine how many rows of data we can show
        m_rowHeight = (int)(1.5 * (double) getFont().getSize());
        m_iconOffsetX = m_rowHeight + 3;
        int rows = height / m_rowHeight;
        m_strings = new DrawnString[rows];
        m_values = new StringValue[rows];
        m_flagColorValues = new IntValue[rows];
        for (int i = 0; i < m_strings.length; i++) {
            m_strings[i] = drawnStringFactory.newDrawnString( "s" + i, 0, 0, Alignment.LEFT, false, getFont(), isFontAntiAliased(), getFontColor() );
            m_values[i] = new StringValue();
            m_flagColorValues[i] = new IntValue();
        }
        if (m_thread == null) {
            m_thread = new Thread(this, "Score Update Thread");
            m_thread.setDaemon(true);
            m_thread.start();
        }
    }
    
    public void run() {
        Reader reader;
        while (true) {
            try {
				reader = new InputStreamReader((new URL(m_serverWidgetEndpoint.getValue())).openStream());
                vehicles = m_gson.fromJson(reader, new TypeToken<ArrayList<Vehicle>>(){}.getType());
                Thread.sleep(UPDATE_DELAY);
            }
            catch (Exception e) {
                log(LogLevel.ERROR, "Error loading JSON data: " + e.getMessage());
            }
        }
    }
    
    @Override
    protected void drawWidget(Clock clock, boolean needsCompleteRedraw, LiveGameData gameData, boolean isEditorMode, TextureImage2D texture, int offsetX,
        int offsetY, int width, int height) {
        ArrayList<Vehicle> veh = vehicles;
        boolean changed = false;
        if (m_lastDrawnDisplay != m_display) {
            m_lastDrawnDisplay = m_display;
            changed = true;
        }
        if (veh != null) {
            int index = 0;
            if (m_values != null) {
                switch (m_display) {
                    case GRID:
                        Collections.sort(veh, new Comparator<Vehicle>() {
                            @Override
                            public int compare(Vehicle v1, Vehicle v2) {
                                if (v1.qualificationPosition < v2.qualificationPosition) {
                                    return -1;
                                }
                                else if (v1.qualificationPosition == v2.qualificationPosition) {
                                    return 0;
                                }
                                else {
                                    return 1;
                                }
                            }});
                        break;
                    case STANDINGS:
                        break;
                }
            	for (int i = 0; i < m_values.length; i++) {
            		StringValue val = m_values[i];
            		IntValue flagVal = m_flagColorValues[i];
                    if (val != null) {
                        if (index < veh.size()) {
                            Vehicle ve = veh.get(index);
                            switch (m_display) {
                                case GRID:
                                    val.update("" + ve.qualificationPosition + " " + ve.driverName + " " + ve.vehicleName);
                                    break;
                                case STANDINGS:
                                    val.update("" + ve.position + " " + ve.positionInClass + " " + ve.driverName + " " + ve.vehicleName);
                                    break;
                            }
                            Color rectColor = Color.BLACK;
                            if (ve.blueFlags > 0) {
                            	rectColor = Color.BLUE;
                            }
                            flagVal.update(rectColor.getRGB());

                        }
                        else {
                            val.update("-");
                            flagVal.update(Color.BLACK.getRGB());
                        }
                        if (val.hasChanged() || flagVal.hasChanged()) {
                            changed = true;
                        }
                    }
                    index++;
                }
            }
        }
        
        boolean redraw = needsCompleteRedraw || changed;
        if (redraw) {
            int index = 0;
            int indexOfViewedCar = 0;
            String vehicleName = "";
            try {
                vehicleName = gameData.getScoringInfo().getViewedVehicleScoringInfo().getVehicleName();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (veh != null) {
                for (Vehicle v : veh) {
                    if (vehicleName.equals(v.vehicleName)) {
                        indexOfViewedCar = index;
                        // show our car in the middle, unless near start or end of list
                        // TODO near end
                        index = Math.max(0, index - (m_rowHeight / 2));
                        break;
                    }
                    index++;
                }
                if (index == veh.size()) {
                    // weird, car not found, show top
                    index = 0;
                    indexOfViewedCar = -1;
                }
            }
            
            int y = 0;
            if (m_values != null) {
            	for (int i = 0; i < m_values.length; i++) {
            		StringValue val = m_values[i];
            		IntValue flagVal = m_flagColorValues[i];
                    if (val != null && val.getValue() != null && index < m_strings.length) {
                        Color color = (index == indexOfViewedCar) ? m_myVehicleColor.getColor() : getFontColor();
                        if (color == null) {
                            color = Color.RED;
                        }
                        m_strings[index].draw(offsetX + m_iconOffsetX, offsetY + y, val.getValue(), color, texture);
                        texture.fillRectangle(new Color(flagVal.getValue()), offsetX + 2, offsetY + y + 2, m_iconOffsetX - 7, m_iconOffsetX - 7, true, null);
                        
                        y += m_rowHeight;
                        index++;
                    }
                }
            }
        }
    }
}
