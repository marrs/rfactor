package net.rfactor.broadcast.controller.impl;

public class Command implements Comparable<Command> {
	private static long m_sequence;
	private final long m_timestamp;
	private final String[] m_args;

	public Command(String... args) {
		if (args == null || args.length == 0) {
			throw new IllegalArgumentException();
		}
		m_args = args;
		m_timestamp = m_sequence++;
	}
	
	public String[] getArgs() {
		return m_args;
	}

	@Override
	public int compareTo(Command other) {
		if (this.equals(other)) {
			return 0;
		}
		boolean thisIsKick = m_args[1].equals("boot");
		boolean otherIsKick = other.getArgs()[1].equals("boot");
		if (thisIsKick && !otherIsKick) {
			return -1;
		}
		if (otherIsKick && !thisIsKick) {
			return 1;
		}
		return (int) (m_timestamp - other.m_timestamp);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Command) {
			String[] otherArgs = ((Command) obj).getArgs();
			if (m_args.length == otherArgs.length) {
				for (int i = 0; i < m_args.length; i++) {
					if (!m_args[i].equals(otherArgs[i])) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int code = 0;
		for (String arg : m_args) {
			code ^= arg.hashCode();
		}
		return code;
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		for (String arg : m_args) {
			if (result.length() > 0) {
				result.append(' ');
			}
			result.append('\'');
			result.append(arg);
			result.append('\'');
		}
		return m_timestamp + ": " + result.toString();
	}
}
