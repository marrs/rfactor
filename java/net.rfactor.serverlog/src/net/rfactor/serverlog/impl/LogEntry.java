package net.rfactor.serverlog.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class LogEntry {
	private final double m_eventTime;
	private final String m_type;
	private final Map<String, Object> m_attributes;
	
	public LogEntry(double eventTime, String type, Map<String, Object> attributes) {
		m_eventTime = eventTime;
		m_type = type;
		m_attributes = new HashMap<String, Object>(attributes);
	}
	
	public double getEventTime() {
		return m_eventTime;
	}
	
	public String getType() {
		return m_type;
	}
	
	public Set<String> getAttributeKeys() {
		return m_attributes.keySet();
	}
	
	public Object getAttributeValue(String key) {
		return m_attributes.get(key);
	}
	
	@Override
	public String toString() {
		return "" + m_eventTime + " - " + m_type;
	}
}