package net.rfactor.serverlog;

import java.util.Map;

public interface ServerLog {
	public void log(double eventTime, String type, Map<String, Object> attributes);
	public void log(double eventTime, String type, Object... attributes);
	public void reset();
}
