package net.rfactor.sync.util;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileSystemView;

public class SystemScanner {
    public static List<File> findRFactorFolders(Progress progress) throws Exception {
        List<File> folders = new ArrayList<File>();
        Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();
        List<Path> scannableRoots = new ArrayList<Path>();
        for (Path root : roots) {
            int type = 0;
            try {
                FileStore store = Files.getFileStore(root);
                Field field = store.getClass().getDeclaredField("volType");
                field.setAccessible(true);
                type = field.getInt(store);
                if (type == 3) {
                    System.out.println("Root: " + root + " type: " + type);
                    scannableRoots.add(root);
                }
            }
            catch (Exception e) {
            }

        }
        int index = 0;
        for (Path root : scannableRoots) {
            double from = (double) index++ * 100.0 / (double) scannableRoots.size();
            double to = (double) index * 100.0 / (double) scannableRoots.size();
            progress.setProgress(from);
            index(root.toFile(), folders, progress, from, to);
        }
        progress.setProgress(100);
        return folders;
    }

    private static void index(File directory, List<File> paths, Progress progress, double from, double to) throws Exception {
//        System.out.println("At " + directory + " F: " + from + " T: " + to);
        File[] files = directory.listFiles();
        if (files == null) {
            System.out.println("No files at " + directory);
            return;
        }
        int index = 0;
        for (File f : files) {
            if (f.isFile()) {
                progress.setProgress(from + (to - from) * ((double) index++ / (double) files.length));
                if (f.getName().toLowerCase().equals("rfactor.exe")) {
                    System.out.println("Found " + f.getAbsolutePath());
                    paths.add(f.getParentFile());
                    return;
                }
            }
        }
        for (File f : files) {
            if (f.isDirectory()) {
                double nestedFrom = from + (to - from) * ((double) index++ / (double) files.length);
                double nestedTo = from + (to - from) * ((double) index / (double) files.length);
                progress.setProgress(nestedFrom);
                index(f, paths, progress, nestedFrom, nestedTo);
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        Progress p = new Progress("Scan");
        List<File> folders = findRFactorFolders(p);
        System.out.println("Found the following rFactor folders:");
        for (File folder : folders) {
            System.out.println(" * " + folder.getAbsolutePath());
        }
    }
}
