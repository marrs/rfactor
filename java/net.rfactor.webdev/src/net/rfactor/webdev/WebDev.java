package net.rfactor.webdev;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.http.HttpService;

@SuppressWarnings("serial")
public class WebDev extends HttpServlet {
	public static final String ENDPOINT = "/dev";

    public void addHttpService(HttpService http) {
        try {
            http.registerServlet(ENDPOINT, this, null, null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeHttpService(HttpService http) {
        http.unregister(ENDPOINT);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	try (FileReader reader = new FileReader(new File("dev", req.getPathInfo()))) {
	    	PrintWriter w = resp.getWriter();
	    	char[] buffer = new char[1024];
	    	int len = reader.read(buffer);
	    	while (len != -1) {
	    		w.write(buffer, 0, len);
	    		len = reader.read(buffer);
	    	}
    	}
    }
}
