package net.rfactor.livescoring.client.endurance.impl;

import java.util.Comparator;

import net.rfactor.livescoring.client.endurance.Vehicle;

/**
 * Compares the qualification positions of the cars.
 */
public class QualificationPositionComparator implements Comparator<Vehicle> {
    public int compare(Vehicle o1, Vehicle o2) {
        return o2.getQualifyPosition() < o1.getQualifyPosition() ? 1 : -1;
    }
}