package net.rfactor.livescoring.client.endurance;

import net.rfactor.util.TimeUtil;

public class VehicleMessage {
    private final double m_et;
    private final String m_message;

    public VehicleMessage(double et, String message) {
        m_et = et;
        m_message = message;
    }
    
    public String toString() {
    	return TimeUtil.toLapTime(m_et, 1) + " " + m_message;
    }
    
    public String getMessage() {
    	return m_message;
    }
    
    public double getTime() {
		return m_et;
	}
}
