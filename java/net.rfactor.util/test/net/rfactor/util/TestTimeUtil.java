package net.rfactor.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTimeUtil {
	@Test
	public void testLapTimeFormatter() {
		assertEquals("1.000", TimeUtil.toLapTime(1));
		assertEquals("1:00.000", TimeUtil.toLapTime(60));
		assertEquals("10:00.000", TimeUtil.toLapTime(10 * 60));
		assertEquals("1:00:00.000", TimeUtil.toLapTime(3600));
		assertEquals("24:00:00.000", TimeUtil.toLapTime(24 * 3600));
		assertEquals("55:00.000", TimeUtil.toLapTime(3300));
		
		assertEquals("1.0", TimeUtil.toLapTime(1, 1));
		assertEquals("1:00.00", TimeUtil.toLapTime(60, 2));
		assertEquals("10:00.000", TimeUtil.toLapTime(10 * 60, 3));
	}

	@Test
	public void testNumberFormatter() {
		assertEquals("5", TimeUtil.format(5, 1, true));
		assertEquals("05", TimeUtil.format(5, 2, true));
		assertEquals("0005", TimeUtil.format(5, 4, true));
		assertEquals(" 5", TimeUtil.format(5, 2, false));
	}
	
	@Test
	public void testConversions() {
		assertEquals(1, TimeUtil.toMinutes(60), 0.001);
		assertEquals(1, TimeUtil.toHours(60 * 60), 0.001);
		assertEquals(100, TimeUtil.toKmh(TimeUtil.toMeterPerSecond(100)), 0.001);
		assertEquals(27.77777, TimeUtil.toMeterPerSecond(100), 0.001);
		assertEquals(100, TimeUtil.toKmh(27.77777), 0.001);
	}
	
	@Test
	public void testTimeAtPoint() {
	    assertEquals(3, TimeUtil.calculateTimeAtPoint(1, 1, 4, 4, 3), 0.001);
        assertEquals(1, TimeUtil.calculateTimeAtPoint(1, 1, 1, 1, 1), 0.001);
        assertEquals(30, TimeUtil.calculateTimeAtPoint(10, 1, 40, 4, 3), 0.001);
        assertEquals(15, TimeUtil.calculateTimeAtPoint(10, 1, 40, 4, 1.5), 0.001);
	}
}
