﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace rF_TVDirector
{
    static class Win32
    {
        [DllImport("user32.dll")]
        internal static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        internal const int KEYEVENTF_KEYUP = 0x2;
        internal const int KEYEVENTF_SCANCODE = 0x0;

        [DllImport("user32.dll")]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);


        [DllImport("user32.dll")]
        internal static extern IntPtr GetForegroundWindow();

        // Activate or minimize a window
        [DllImportAttribute("User32.DLL")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        internal const int SW_SHOW = 5;
        internal const int SW_MINIMIZE = 6;
        internal const int SW_RESTORE = 9;

    }
}
