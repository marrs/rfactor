﻿namespace rF_TVDirector
{
    partial class frmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.timerPos = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblOwnPos = new System.Windows.Forms.Label();
            this.lblNewPos = new System.Windows.Forms.Label();
            this.lblErrors = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemoteAccess = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mynotifyicon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuShow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
            this.contextmenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerPos
            // 
            this.timerPos.Enabled = true;
            this.timerPos.Tick += new System.EventHandler(this.timerPos_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Position of own vehicle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Position to switch to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Errors";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(180, 188);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblOwnPos
            // 
            this.lblOwnPos.BackColor = System.Drawing.Color.Black;
            this.lblOwnPos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOwnPos.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOwnPos.ForeColor = System.Drawing.Color.Lime;
            this.lblOwnPos.Location = new System.Drawing.Point(128, 5);
            this.lblOwnPos.Name = "lblOwnPos";
            this.lblOwnPos.Size = new System.Drawing.Size(55, 19);
            this.lblOwnPos.TabIndex = 7;
            this.lblOwnPos.Text = "--";
            this.lblOwnPos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNewPos
            // 
            this.lblNewPos.BackColor = System.Drawing.Color.Black;
            this.lblNewPos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNewPos.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewPos.ForeColor = System.Drawing.Color.Lime;
            this.lblNewPos.Location = new System.Drawing.Point(128, 30);
            this.lblNewPos.Name = "lblNewPos";
            this.lblNewPos.Size = new System.Drawing.Size(55, 19);
            this.lblNewPos.TabIndex = 8;
            this.lblNewPos.Text = "--";
            this.lblNewPos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblErrors
            // 
            this.lblErrors.BackColor = System.Drawing.Color.Black;
            this.lblErrors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrors.ForeColor = System.Drawing.Color.White;
            this.lblErrors.Location = new System.Drawing.Point(9, 95);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(246, 35);
            this.lblErrors.TabIndex = 9;
            this.lblErrors.Text = "Errors";
            // 
            // lblPort
            // 
            this.lblPort.BackColor = System.Drawing.Color.Black;
            this.lblPort.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPort.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPort.ForeColor = System.Drawing.Color.Lime;
            this.lblPort.Location = new System.Drawing.Point(128, 54);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(55, 19);
            this.lblPort.TabIndex = 11;
            this.lblPort.Text = "81";
            this.lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Listening on Port";
            // 
            // txtRemoteAccess
            // 
            this.txtRemoteAccess.Location = new System.Drawing.Point(9, 162);
            this.txtRemoteAccess.Name = "txtRemoteAccess";
            this.txtRemoteAccess.ReadOnly = true;
            this.txtRemoteAccess.Size = new System.Drawing.Size(246, 20);
            this.txtRemoteAccess.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Remote access key (Copy && Paste):";
            // 
            // mynotifyicon
            // 
            this.mynotifyicon.BalloonTipText = "rF_TVDirector is still running";
            this.mynotifyicon.ContextMenuStrip = this.contextmenu;
            this.mynotifyicon.Icon = ((System.Drawing.Icon)(resources.GetObject("mynotifyicon.Icon")));
            this.mynotifyicon.Text = "rF_TVDirector";
            // 
            // contextmenu
            // 
            this.contextmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShow,
            this.mnuClose});
            this.contextmenu.Name = "contextmenu";
            this.contextmenu.ShowImageMargin = false;
            this.contextmenu.Size = new System.Drawing.Size(79, 48);
            // 
            // mnuShow
            // 
            this.mnuShow.Name = "mnuShow";
            this.mnuShow.Size = new System.Drawing.Size(78, 22);
            this.mnuShow.Text = "Show";
            this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
            // 
            // mnuClose
            // 
            this.mnuClose.Name = "mnuClose";
            this.mnuClose.Size = new System.Drawing.Size(78, 22);
            this.mnuClose.Text = "Close";
            this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 223);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRemoteAccess);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblErrors);
            this.Controls.Add(this.lblNewPos);
            this.Controls.Add(this.lblOwnPos);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmMain";
            this.Tag = "rF_TVDirector Status";
            this.Text = "rF_TVDirector Status";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.contextmenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerPos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblOwnPos;
        private System.Windows.Forms.Label lblNewPos;
        private System.Windows.Forms.Label lblErrors;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRemoteAccess;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NotifyIcon mynotifyicon;
        private System.Windows.Forms.ContextMenuStrip contextmenu;
        private System.Windows.Forms.ToolStripMenuItem mnuShow;
        private System.Windows.Forms.ToolStripMenuItem mnuClose;

    }
}

